package api;

import java.io.File;

import model.data_structures.IStack;
import model.exceptions.TripNotFoundException;
import model.vo.StopVO;

public interface ISTSManager {

	/**
	 * Reads a file in json format containing bus updates in real time
	 * @param rtFile - The file to read
	 * @throws TripNotFoundException 
	 */
	public void readBusUpdate(File rtFile) throws TripNotFoundException;
	
	public IStack<StopVO> listStops (Integer tripID);

	public void loadStops();
}
