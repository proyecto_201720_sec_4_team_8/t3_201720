package model.data_structures;

import java.util.Iterator;

import model.vo.StopVO;

public class Pila<T> implements Iterable<T>, IStack<T>
{
	private Nodo primero = null;
	
	private class Nodo
	{
		T elemento;
		Nodo siguiente;
	}
	
	private class ListIterator implements Iterator<T>
	{
		private Nodo actual = primero;
		
		public boolean hasNext()
		{
			return actual != null;
		}
		
		public T next()
		{
			T objeto = actual.elemento;
			actual = actual.siguiente;
			return objeto;
		}
	}
	
	public boolean isEmpty()
	{
		return primero == null;
	}
	
	public int size()
	{
		int rta = 0;
		Iterator<T> iter = iterator();
		while(iter.hasNext())
		{
			iter.next();
			rta++;
		}
		return rta;
	}
	
	public T get(int i) throws Exception
	{
		if(i < 0 || i > size())
		{
			throw new Exception();
		}
		Nodo rta = primero;
		int pos = 0;
		while(rta != null && pos < i)
		{
			rta = rta.siguiente;
			pos++;
		}
		return rta.elemento;
	}
	
	public Iterator<T> iterator()
	{
		return new ListIterator();
	}

	public void push(Object agregar)
    {
        Nodo agregado = new Nodo();
        agregado.elemento = (T) agregar;
        Nodo oldFirst = primero;
        primero = agregado;
        primero.siguiente = oldFirst;
    }
	
	
	public T pop()
    {
        T item = primero.elemento;
        primero = primero.siguiente;
        return item;
    }
}