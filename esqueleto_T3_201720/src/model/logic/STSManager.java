package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import api.ISTSManager;
import model.data_structures.Cola;
import model.data_structures.IStack;
import model.data_structures.Pila;
import model.exceptions.TripNotFoundException;
import model.vo.BusUpdateVO;
import model.vo.StopVO;

public class STSManager implements ISTSManager
{
	private Cola<StopVO> listaParadas;
	private Cola <BusUpdateVO> listaUpdates;

	public void readBusUpdate(File rtFile) throws TripNotFoundException
	{

		BufferedReader reader = null;
		
		try {
			Cola<BusUpdateVO> cola = new Cola<BusUpdateVO>();
			
			reader = new BufferedReader(new FileReader(rtFile));

			Gson gson = new GsonBuilder().create();

			BusUpdateVO []update = gson.fromJson(reader, BusUpdateVO[].class);

			JSONParser parser = new JSONParser();

			JSONArray a = (JSONArray) parser.parse(new FileReader(rtFile));

			ArrayList <String > vehicleNo = new ArrayList<String>();
			ArrayList <String > recs = new ArrayList<String>();
			ArrayList <String > dest = new ArrayList<String>();
			ArrayList <String > trips = new ArrayList<String>();
			ArrayList <String > routesMap = new ArrayList<String>();
			ArrayList <String > routesNo = new ArrayList<String>();
			ArrayList <String > patterns = new ArrayList<String>();
			ArrayList <String > direc = new ArrayList<String>();
			ArrayList <String > lat = new ArrayList<String>();
			ArrayList <String > lon = new ArrayList<String>();


			for (Object o : a)
			{
				JSONObject jsonObject = (JSONObject) o;

				String tripId =  String .valueOf(jsonObject.get("TripId"));
				trips.add(tripId);

				String latitude = jsonObject.get("Latitude")+"";
				lat.add(latitude);

				String longitude =  jsonObject.get("Longitude")+"";
				lon.add(longitude);

				String vehicleNumber = (String) jsonObject.get("VehicleNo");
				vehicleNo.add(vehicleNumber); 


				String routeNumber =  (String) jsonObject.get("RouteNo");
				routesNo.add(routeNumber);

				String direction = (String) jsonObject.get("Direction");
				direc.add(direction);


				String pattern = (String) jsonObject.get("Pattern");
				patterns.add(pattern);

				JSONObject routeMap =  (JSONObject) jsonObject.get("RouteMap");
				String routeMap1 = (String) routeMap.get("Href");
				routesMap.add(routeMap1);

				String record = (String) jsonObject.get("RecordedTime");
				recs.add(record);

				String destination = (String) jsonObject.get("Destination");
				dest.add(destination);
			}

			for(int i =0;i<update.length;i++)
			{
				update[i].setDest(dest.get(i));
				update[i].setDirec(direc.get(i));
				update[i].setLat(lat.get(i));
				update[i].setLon(lon.get(i));
				update[i].setPattern(patterns.get(i));
				update[i].setRec(recs.get(i));
				update[i].setRouteMap(routesMap.get(i));
				update[i].setTrip(trips.get(i));
				update[i].setVehicleNo(vehicleNo.get(i));	
				update[i].setRouteNo(routesNo.get(i));	
				cola.enqueue(update[i]);
				
			}
             listaUpdates = cola;
		System.out.println(update[0].toString());
		}

		catch ( FileNotFoundException ex)
		{

			ex.printStackTrace();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public IStack<StopVO> listStops(Integer tripID) 
	{
		
	    
	        String IDTrip = tripID+"";
	        Cola<BusUpdateVO> listaTripID = new Cola<BusUpdateVO>();
	        IStack<StopVO> rta = new Pila<StopVO>();
	        for(int i = 0; i < listaUpdates.size(); i++)
	        {
	            BusUpdateVO busU = listaUpdates.get(i);
	            if(busU.darTripID().equals(IDTrip))
	            {
	                listaTripID.enqueue(busU);
	            }
	        }
	       
	        for(int i = 0; i < listaTripID.size(); i++)
	        {
	            BusUpdateVO bus = listaTripID.get(i);
	            for(int j = 0; j < listaParadas.size(); j++)
	            {
	                StopVO stop = listaParadas.get(j);
	              
	                if(getDistance(Double.parseDouble(bus.darLat()), Double.parseDouble(bus.darLon()), Double.parseDouble(stop.darLat()), Double.parseDouble(stop.darLon())) <= 70)
	                {
	                    rta.push(stop);
	                }
	            }
	        }
	       
	        return rta;    
	}
	
	   public double getDistance(double lat1, double lon1, double lat2, double lon2)
	    {
	        final int R = 6371*1000; // Radious of the earth

	        Double latDistance = toRad(lat2-lat1);
	        Double lonDistance = toRad(lon2-lon1);
	        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
	                Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
	                Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
	        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	        Double distance = R * c;

	        return distance;

	    }
	
	   private Double toRad(Double value) 
	   {
	        return value * Math.PI / 180;
	    }

	public void loadStops()
	{
		String csvFile = "data/stops.txt";
		BufferedReader br = null;
		String line = "";
		try
		{
			Cola<StopVO> lista = new Cola<StopVO>();
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null)
			{
				String[] datos = line.split(",");
				if(!(datos[0].equals("stop_id")))
				{
					StopVO agregar = new StopVO(datos[0], datos[1], datos[2], datos[3], datos[4], datos[5], datos[6]);
					lista.enqueue(agregar);
				}	
			}
			listaParadas = lista;
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally 
		{
			if (br != null)
			{
				try
				{
					br.close();
				}
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}			
}
