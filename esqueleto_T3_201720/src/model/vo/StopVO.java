package model.vo;

public class StopVO
{
	private String id;
	private String code;
	private String name;
	private String desc;
	private String lat;
	private String lon;
	private String zoneID;
	
	public StopVO(String pId, String pCode, String pName, String pDesc, String pLat ,String pLon, String pZoneID)
	{
		id = pId;
		code = pCode;
		name = pName;
		desc = pDesc;
		lat = pLat;
		lon = pLon;
		zoneID = pZoneID;
	}
	
	public String darId()
	{
		return id;
	}
	
	public String darCode()
	{
		return code;
	}
	
	public String darName()
	{
		return name;
	}
	
	public String darDesc()
	{
		return desc;
	}
	
	public String darLat()
	{
		return lat;
	}
	
	public String darLon()
	{
		return lon;
	}
	
	public String darZoneID()
	{
		return zoneID;
	}
}