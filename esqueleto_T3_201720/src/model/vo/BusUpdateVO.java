package model.vo;

public class BusUpdateVO
{
	private String vehicleNO;
	private String tripID;
	private String routeNO;
	private String lat;
	private String lon;
	private String direc;
	private String pattern;
	private String rec;
	private String routeMap;
	private String dest;

	public String darVehicleNO()
	{
		return vehicleNO;
	}
	
	public String darTripID()
	{
		return tripID;
	}
	
	public String darRouteNO()
	{
		return routeNO;
	}
	
	public String darLat()
	{
		return lat;
	}
	
	public String darLon()
	{
		return lon;
	}
	public String darRouteMap()
	{
		return routeMap;
	}
	public String darPattern()
	{
		return pattern;
	}
	public String darDirec()
	{
		return direc;
	}
	public String darRec()
	{
		return rec;
	}
	public String darDest()
	{
		return dest;
	}
	
	public String toString() 
	 {
	      
		return "Update{" + "VehicleNo=" + vehicleNO + ", TripId=" + tripID + ", RouteNo=" + routeNO + ", Direction=" + direc + ", Destination=" + dest + ", Pattern=" + pattern + ", Latitude=" + lat + ",Longitude=" + lon + ", RecordedTime=" + rec + ", RouteMap=" + routeMap + '}';
	      
	 }

	public void setVehicleNo(String string)
	{
		vehicleNO = string;
		
	}
	public void setPattern(String string)
	{
		pattern = string;
		
	}
	public void setRec(String string)
	{
		rec = string;
	}
	public void setDest(String string)
	
	{
		dest = string;
	}
	public void setDirec(String string)
	{
		direc = string;		
	}
	public void setRouteMap(String string)
	{
		routeMap = string;		
	}
	public void setLat(String string)
	{
		lat= string;
		
	}
	public void setLon(String string)
	{
		lon = string;
		
	}
	public void setTrip(String string)
	{
		tripID = string;
		
	}
	public void setRouteNo(String string)
	{
		routeNO = string;
		
	}
}
